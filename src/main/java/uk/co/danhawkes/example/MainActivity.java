package uk.co.danhawkes.example;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;

import com.example.alertdialogue.R;

public class MainActivity extends Activity {

	private static final String TITLE = "Warning";
	private static final String MESSAGE = "A network error occurred.";
	private static final String CANCEL = "Cancel";
	private static final String OK = "OK";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		showFake(null);
	}

	public void showReal(View v) {
		DialogFragment testFragment = new DialogFragment() {

			@Override
			public Dialog onCreateDialog(Bundle savedInstanceState) {
				android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(
						MainActivity.this);
				builder.setTitle(TITLE);
				builder.setMessage(MESSAGE);
				builder.setPositiveButton(OK, null);
				builder.setNegativeButton(CANCEL, null);

				return builder.create();
			}
		};
		testFragment.show(getFragmentManager(), "");
	}

	public void showFake(View v) {
		DialogFragment testFragment = new DialogFragment() {

			@Override
			public Dialog onCreateDialog(Bundle savedInstanceState) {
				uk.co.danhawkes.AlertDialog.Builder builder = new uk.co.danhawkes.AlertDialog.Builder(
						MainActivity.this);
				builder.setTitle(TITLE);
				builder.setMessage(MESSAGE);
				builder.setPositiveButton(OK, null);
				builder.setNegativeButton(CANCEL, null);

				return builder.create();
			}
		};
		testFragment.show(getFragmentManager(), "");
	}
}
